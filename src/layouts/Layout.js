import React, { useState, useEffect, useRef } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { useLocation } from "react-router-dom";

import history from "../utils/history";
import "./Layout.scss";


const Layout = (props) => {
  return (
    <div id="Layout" >

    </div>
  );
};

const mapStateToProps = (state) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Layout);
