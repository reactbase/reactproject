import React, { useState, useEffect } from "react";
import _, { includes } from "lodash";
import { connect } from "react-redux";
import history from "../../utils/history";
import "./index.scss";


const Login = (props) => {

  useEffect(() => {
  }, []);


  return (
    <div className="home">

    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    api: _.get(state, "api.api", []),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    GET_API(callback, loading) {
      dispatch({ type: "GET_API", callback, loading });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
