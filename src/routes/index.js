import _ from "lodash";
import LayoutRoute from "../layouts/LayoutRoute";

import Login from "../pages/Login/index";


export default [
  {
    key: "/",
    path: "/",
    exact: true,
    component: Login,
    layout: LayoutRoute,
    title: "",
  },
];
