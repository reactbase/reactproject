import request from "../utils/request";

export function GET_API(payload) {
  return request.get("/api", payload);
}

