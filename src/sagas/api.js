import { put, takeLatest, call, all } from "redux-saga/effects";
import {
  GET_API,
} from "../services/api";
import { handleError } from "../utils/error";
import history from "../utils/history";

function* GET_APIEffect({ payload, callback, loading }) {
  try {
    if (loading) loading(true);

    const Result = yield call(GET_API, payload);
    yield put({ type: "SAVE_API", payload: Result });

    if (loading) loading(false);
    if (callback) callback();
  } catch (error) {
    if (loading) loading(false);
    yield handleError(error);
  }
}


export default function* Example() {
  yield takeLatest("GET_API", GET_APIEffect);
}
