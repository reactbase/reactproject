export default (state = {}, { type, payload }) => {
  switch (type) {
    case "SAVE_API":
      return { ...state, API: payload };
    default:
      return state;
  }
};
